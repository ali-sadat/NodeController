from __future__ import absolute_import
from os import path, environ
import json
from flask import Flask, Blueprint, abort, jsonify, request, session
import settings
import pprint
import time

#from celery.canvas import signature
from celery import Celery, chain, group, subtask, signature

app = Flask(__name__)
app.config.from_object(settings)

def make_celery(app):
    celery = Celery(app.import_name, broker='amqp://guest@172.18.0.111:5672') #, backend='redis://172.18.0.106:6379/0')
    celery.conf.update(app.config)

    celery.conf.task_default_queue='for_task_B'
    celery.conf.update(
           task_default_exchange='for_task_B',
           task_default_exchange_type='direct',
           task_default_queue='for_task_B',
           task_default_routing_key='for_task_',
           result_expires=60,
           task_acks_late=True,
           broker_url='amqp://guest:guest@172.18.0.111:5672',
           #result_backend='redis://172.18.0.106:6379/0',
           enable_utc = True,
           timezone = 'UTC',
           task_soft_time_limit = 3600,
           task_time_limit = 3600

    )

    TaskBase = celery.Task
    class ContextTask(TaskBase):
        abstract = True
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)
    celery.Task = ContextTask
    return celery

celery = make_celery(app)

@celery.task(name="tasks.c_empty", max_retries=3)
def c_empty(chain_var=None):
    pass

@celery.task(name="tasks.add", max_retries=3)
def add(x, y):
    pass

@celery.task(name="tasks.sub", max_retries=3) 
def sub(x, y):
    pass

@celery.task(name="tasks.mul", max_retries=3)
def mul(x, y):
    pass

@app.route("/add")
def add_two_values(x=16, y=16):
    x = int(request.args.get("x", x))
    y = int(request.args.get("y", y))
    res = add.apply_async((x, y))
    context = {"id": res.task_id, "x": x, "y": y}
    result = "add((x){}, (y){})".format(context['x'], context['y'])
    goto = "{}".format(context['id'])
    return jsonify(result=result, goto=goto)

@app.route("/sub")
def sub_two_values(x=16, y=16):
    x = int(request.args.get("x", x))
    y = int(request.args.get("y", y))
    res = sub.apply_async((x, y))
    context = {"id": res.task_id, "x": x, "y": y}
    result = "add((x){}, (y){})".format(context['x'], context['y'])
    goto = "{}".format(context['id'])
    return jsonify(result=result, goto=goto)

@app.route("/add_n_m2_n_sub")
def add_n_m2_n_sub_fun(x=16, y=16):
    x = int(request.args.get("x", x))
    y = int(request.args.get("y", y))
    job = group([
                       	chain(	add.s(x,y), 	add.s(y), 	c_empty.s(), 	c_empty.s(), 	c_empty.s()	),	#.apply_async(),
                        chain(	c_empty.s(), 	c_empty.s(), 	c_empty.s(), 	c_empty.s(), 	c_empty.s()	),	#.apply_async(),
                       	chain(	c_empty.s(), 	c_empty.s(), 	c_empty.s(), 	c_empty.s(), 	c_empty.s()	),	#.apply_async(),
                       	chain(	c_empty.s(), 	c_empty.s(), 	c_empty.s(), 	c_empty.s(), 	c_empty.s()	),	#.apply_async(),
                        chain(  c_empty.s(),    c_empty.s(),    c_empty.s(),    c_empty.s(),    c_empty.s()     ),	#.apply_async(),
               	])
    res = job.apply_async()
    return jsonify(job)

@app.route("/test")
def hello_world(x=16, y=16):
    x = int(request.args.get("x", x))
    y = int(request.args.get("y", y))
    res = sub.apply_async((x, y))
    context = {"id": res.task_id, "x": x, "y": y}
    #print ("task_id: {}".format(res.task_id))
    result = "add((x){}, (y){})".format(context['x'], context['y'])
    goto = "{}".format(context['id'])
    return jsonify(result=result, goto=goto)

@app.route("/test/result/<task_id>")
def show_result(task_id):
    retval = add.AsyncResult(task_id).get(timeout=1.0)
    return repr(retval)

if __name__ == "__main__":
    port = int(environ.get("PORT", 5000))
    app.run(host='0.0.0.0', port=port, debug=True)


