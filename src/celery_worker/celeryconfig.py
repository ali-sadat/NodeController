

# List of modules to import when the Celery worker starts.
#imports = ('celery_worker.tasks',)

#task_annotations = {'tasks.add': {'rate_limit': '10/s'}}

broker_url = 'amqp://guest@172.18.0.111:5672'
result_backend = 'redis://172.18.0.106:6379/0'

task_default_exchange = 'for_task_B'
task_default_exchange_type = 'direct'
task_default_queue = 'for_task_B'
task_default_routing_key = 'for_task_B'
worker_concurrency = 5

#http://docs.celeryproject.org/en/latest/userguide/configuration.html#example-configuration-file
#celery worker --config=celeryconfig.py --loader=myloader.Loader
#https://github.com/celery/celery/search?utf8=%E2%9C%93&q=celeryconfig.py&type=



