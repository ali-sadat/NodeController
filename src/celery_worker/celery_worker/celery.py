
from celery import Celery, group, chain, subtask, signature
from celery.canvas import signature
import time

def make_celery():
    celery = Celery(__name__) 
    celery.conf.update(
           task_default_exchange='for_task_B',
           task_default_exchange_type='direct',
           task_default_queue='for_task_B',
           task_default_routing_key='for_task_',
           result_expires=60,
           task_acks_late=True,
           broker_url='amqp://guest:guest@172.18.0.111:5672', 
           result_backend='redis://172.18.0.106:6379/0',
           enable_utc = True,
           timezone = 'UTC',
           task_soft_time_limit = 3600,
           task_time_limit = 3600

    )
    return celery

celery = make_celery()

def backoff(attempts):
    """Return a backoff delay, in seconds, given a number of attempts.

    The delay increases very rapidly with the number of attemps:
    1, 2, 4, 8, 16, 32, ...

    """
    return 1** attempts

@celery.task(name="tasks.c_empty",max_retries=3)
def c_empty(chain_var=None):
    return True

@celery.task(name="tasks.add",max_retries=3)
def add(x, y):
    a = x + y
    #time.sleep( 5 )
    print ("add: {} ".format(a))
    return a

@celery.task(name="tasks.sub", max_retries=3)
def sub(x, y):
    #print ("Sub delay - Start : %s" % time.ctime())
    #time.sleep( 5 )
    #print ("Sub delay - End : %s" % time.ctime())
    a = x - y
    print ("sub: {} ".format(a))
    return a

@celery.task(name="tasks.mul", max_retries=3)
def mul(x, y):
    p = x * y
    print ("mul: {} ".format(a))
    return a


